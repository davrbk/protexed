package com.clearsofware.protexed.server;

import android.util.Log;

import com.clearsofware.protexed.AppConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * Created by Pavel on 5/8/2015.
 */
public class HttpUtils {
    private static final String TAG = "HttpUtils";

    public static HttpResponse get(final String url) {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Accept", "application/json");
        if (AppConfig.DEBUG)
            Log.d(TAG, "URL " + url);
        try {
            HttpResponse response = new DefaultHttpClient().execute(httpGet);

            if (AppConfig.DEBUG)
                Log.d(TAG, "RESPONSE " + EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HttpResponse post(final String url, String entity, String contentType) {
        HttpPost httpPost = new HttpPost(url);
        if (AppConfig.DEBUG) {
            Log.d(TAG, "URL " + url);
//            Log.d(TAG, "REQUEST " + entity);
        }
        try {
            httpPost.setHeader("Content-type", contentType);
            httpPost.setEntity(new StringEntity(entity, HTTP.UTF_8));
            HttpResponse response = new DefaultHttpClient().execute(httpPost);
            if (AppConfig.DEBUG)
                Log.d(TAG, "RESPONSE " + EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
