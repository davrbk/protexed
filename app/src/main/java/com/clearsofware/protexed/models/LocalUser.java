package com.clearsofware.protexed.models;

/**
 * Created by Pavel on 4/23/2015.
 */
public class LocalUser {

    // temporary data for Login Activity. will be used in authorization
    String user;

    public LocalUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
