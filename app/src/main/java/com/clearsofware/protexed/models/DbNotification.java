package com.clearsofware.protexed.models;

/**
 * Created by Pavel on 4/30/2015.
 */
public class DbNotification {

    public static final String APP_PHONE = "com.clearsofware.protexed.models.DbNotification.APP_PHONE";

    public static final String APP_SMS = "com.clearsofware.protexed.models.DbNotification.APP_SMS";

    String appPackage;
    String ticker;
    Long time;
    boolean isRead;

    public DbNotification() {

    }

    public DbNotification(String appPackage, String ticker, Long time) {
        this.appPackage = appPackage;
        this.ticker = ticker;
        this.time = time;
    }

    public DbNotification(String appPackage, String ticker, long time, int isRead) {
        this.appPackage = appPackage;
        this.ticker = ticker;
        this.time = time;
        if (isRead == 0)
            this.isRead = false;
        else
            this.isRead = true;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    public void setRead(int isRead) {
        if (isRead == 0)
            this.isRead = false;
        else
            this.isRead = true;
    }
}
