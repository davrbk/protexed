package com.clearsofware.protexed.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.clearsofware.protexed.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Pavel on 5/8/2015.
 */
public class NetworkUtils {

    public static boolean isOnline(Context context, boolean withToast) {
        if (!hasInternetConnectivity(context)) {
            if (withToast)
                Toast.makeText(context, context.getResources().getString(R.string.error_internet_connection), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private static boolean hasInternetConnectivity(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
