package com.clearsofware.protexed.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import com.clearsofware.protexed.R;

/**
 * Created by Pavel on 4/23/2015.
 */
public class DisplayUtils {

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int dpToPx(Context context, int dp) {
        return (int) ((dp * context.getResources().getDisplayMetrics().density) + 0.5);
    }

    public static int dpToPx(Context context, float dp) {
        return (int) ((dp * context.getResources().getDisplayMetrics().density) + 0.5);
    }

    public static int getActionBarHeight(Context context) {
        int[] textSizeAttr = new int[] { android.R.attr.actionBarSize, R.attr.actionBarSize };
        TypedArray a = context.obtainStyledAttributes(new TypedValue().data, textSizeAttr);
        float heightMaterial = a.getDimension(1, -1);
        return (int) heightMaterial;
    }
}
