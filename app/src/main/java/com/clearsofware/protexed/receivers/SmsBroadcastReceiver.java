package com.clearsofware.protexed.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.database.DBHelper;
import com.clearsofware.protexed.fragments.SettingsFragment;
import com.clearsofware.protexed.models.DbNotification;

/**
 * Created by david on 4/23/15.
 */
public class SmsBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false)
                && ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false)) {

            // Retrieves a map of extended data from the intent.
            final Bundle bundle = intent.getExtras();

            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (int i = 0; i < pdusObj.length; i++) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String senderNum = currentMessage.getDisplayOriginatingAddress();
                        String message = currentMessage.getDisplayMessageBody();
                        if (AppConfig.DEBUG)
                            Log.d("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                        DbNotification sms = new DbNotification(DbNotification.APP_SMS, senderNum + "," + message,
                                System.currentTimeMillis());
                        DBHelper.getInstance(context).addNotification(sms);

                        if(ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true)) {
                            SettingsFragment.sendSMS(senderNum, context.getString(R.string.sms_protexed));
                        } else {
//                        String parentPhone = ProtexedApp.getInstance().getSharedPreferences()
//                                .getString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, "");
//
//                        String autorespMessage = ProtexedApp.getInstance().getSharedPreferences()
//                                .getString(AppConfig.Prefs.AUTORESPONSE_MESSAGE, context.getString(R.string.sms_protexed));
//
//                        if(parentPhone != null && !parentPhone.equals("")) {
//                            SettingsFragment.sendSMS(parentPhone, autorespMessage);
//                        }
                        }

                        abortBroadcast();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
