package com.clearsofware.protexed.receivers;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by david on 4/29/15.
 */
public class PopupsBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if(action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {

            ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);

            String className = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
            Log.d("Running window class:", className);

        }
    }
}