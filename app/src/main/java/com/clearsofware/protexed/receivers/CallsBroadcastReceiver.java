package com.clearsofware.protexed.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.android.internal.telephony.ITelephony;
import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.database.DBHelper;
import com.clearsofware.protexed.fragments.SettingsFragment;
import com.clearsofware.protexed.models.DbNotification;

/**
 * Created by david on 4/22/15.
 */
public class CallsBroadcastReceiver extends BroadcastReceiver {


    private TelephonyManager mTelephonyManager;

    private ITelephony mTelephonyService;

    private Context mContext;

    public CallsBroadcastReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        mContext = context;

        mTelephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

        if(ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false)
                && ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_CALLS_RECEIVER_RUNNING, false)) {

            //Java Reflections
            Class c = null;

            try {
                c = Class.forName(mTelephonyManager.getClass().getName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Method m = null;
            try {
                m = c.getDeclaredMethod("getITelephony");
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            m.setAccessible(true);
            try {
                mTelephonyService = (ITelephony) m.invoke(mTelephonyManager);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            mTelephonyManager.listen(callBlockListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    PhoneStateListener callBlockListener = new PhoneStateListener() {

        public void onCallStateChanged(int state, String incomingNumber) {

            if(state == TelephonyManager.CALL_STATE_RINGING) {

                try {

                    mTelephonyService.silenceRinger();
                    mTelephonyService.endCall();
                    DbNotification call = new DbNotification(DbNotification.APP_PHONE, incomingNumber, System.currentTimeMillis());
                    DBHelper.getInstance(mContext).addNotification(call);

                    if(ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true)) {
                        SettingsFragment.sendSMS(incomingNumber, mContext.getString(R.string.sms_protexed));
                    } else {
//                        String parentPhone = ProtexedApp.getInstance().getSharedPreferences()
//                                .getString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, "");
//
//                        String message = ProtexedApp.getInstance().getSharedPreferences()
//                                .getString(AppConfig.Prefs.AUTORESPONSE_MESSAGE, mContext.getString(R.string.sms_protexed));
//
//                        if(parentPhone != null && !parentPhone.equals("")) {
//                            SettingsFragment.sendSMS(parentPhone, message);
//                        }
                    }

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {

                try {
                    mTelephonyService.silenceRinger();
                    mTelephonyService.endCall();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
