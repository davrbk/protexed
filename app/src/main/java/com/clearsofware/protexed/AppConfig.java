package com.clearsofware.protexed;

/**
 * Created by Pavel on 4/22/2015.
 */
public class AppConfig {
    public static final boolean DEBUG = true;

    public static class Location {
        // The minimum distance to change updates in meters
        public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;

        // The minimum time between updates in milliseconds
        public static final long MIN_TIME_BW_UPDATES = 3000;
    }

    public static class Prefs {
        public static final String PREFS_NAME = "ProtexedApp";
        public static final String LOCAL_USER = "localUser";
        public static final String SMS_DEFAULT_APP_PACKAGE = "SmsDefaultAppPackage";
        public static final String IS_CALLS_RECEIVER_RUNNING = "IsCallsReceiverRunning";
        public static final String IS_SMS_RECEIVER_RUNNING = "IsSmsReceiverRunning";
        public static final String IS_NOTIFICATIONS_RECEIVER_RUNNING = "IsNotificationsReceiverRunning";
        public static final String IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING = "IsEmailNotificationsReceiverRunning";
        public static final String IS_PROTEXED_RUNNING = "IsProtexedRunning";
        public static final String IS_AUTORESPONSE_RUNNING = "IsAutoResponseRunning";
        public static final String AUTOSTART_SPEED_IN_MPH = "AutostartSpeedInMPh";
        public static final String PROTEXED_DRIVEN_MILES = "ProtexedDrivenMiles";
        public static final String EMAIL_DEFAULT_APP_PACKAGE = "EmailDefaultAppPackage";
        public static final String LOGGED = "logged";
        public static final String AUTORESPONSE_MESSAGE = "AutoResponseMessage";
        public static final String USER_ID = "UserId";
        public static final String USER_LOGIN = "UserLogin";
        public static final String USER_PAS = "UserPassword";
        public static final String USER_AGE = "UserAge";
        public static final String USER_SEX = "UserSex";
        public static final String USER_GUARDIAN_PARENT_PHONE = "GuardianParentPhone";
        public static final String USER_ABOUT = "UserAbout";
        public static final String USER_CITY = "UserCity";
        public static final String USER_STATE = "UserState";
        public static final String USER_REF_CODE = "UserRefCode";
    }
}