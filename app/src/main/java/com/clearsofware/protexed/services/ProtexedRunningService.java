package com.clearsofware.protexed.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.test.ServiceTestCase;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.activities.MainActivity;
import com.clearsofware.protexed.fragments.SettingsFragment;
import com.clearsofware.protexed.location.GPSTracker;
import com.clearsofware.protexed.location.GetLocation;
import com.clearsofware.protexed.location.LocationListener;
import com.clearsofware.protexed.objects.UserInfo;

/**
 * Created by Pavel on 4/27/2015.
 */
public class ProtexedRunningService extends Service {

    public static final String TAG = "ProtexedRunningService";
    public static final String ACTION_DISMISS = "com.clearsofware.protexed.services.ProtexedRunningService.ACTION_DISMISS";

    private GPSTracker mGpsTracker;
    private static boolean receiversAreWorking = false;
    private static boolean serviceIsWorking = false;
    private static boolean isSmsSent = false;
    private DismissReceiver mDismissReceiver;

    public static boolean isServiceWorking() {
        return serviceIsWorking;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        startForeground(R.string.app_name, initNotification(this, "Service is stopped.\nCurrent speed 0 m/ph"));

        if (AppConfig.DEBUG)
            Log.d(TAG, "is started");

        mDismissReceiver = new DismissReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_DISMISS);
        registerReceiver(mDismissReceiver, filter);

        mGpsTracker = new GPSTracker(this, new GPSTracker.OnTrackerChangedListener() {
            @Override
            public void onProviderDisabled(String provider) {
//                showSettingsAlert();
            }

            @Override
            public void onLocationChanged(double lat, double lng) {
                new GetLocation(ProtexedRunningService.this, mGpsTracker, mLocationListener, false).execute();
            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        });

        LocationManager locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new GetLocation(this, mGpsTracker, mLocationListener, false).execute();
        } else {
//                showSettingsAlert();
        }

        serviceIsWorking = true;

        super.onCreate();
    }

    @Override
    public void onDestroy() {
        if (AppConfig.DEBUG)
            Log.d(TAG, "is stopped");

        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false);
        editor.remove(AppConfig.Prefs.PROTEXED_DRIVEN_MILES);
        editor.commit();

        serviceIsWorking = false;
        receiversAreWorking = false;
        if (mGpsTracker != null) {
            mGpsTracker.stopUsingGPS();
        }
        unregisterReceiver(mDismissReceiver);
        super.onDestroy();
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationReceived(double speed, double distance, double lat, double lng) {
            if (AppConfig.DEBUG) {
                Log.d(TAG, "Current speed is " + speed + " m/ph");
            }

            String msgDistance = "";

            int remainingDistance = ProtexedApp.getInstance().getSharedPreferences().getInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, -100);
            if (remainingDistance > -100) {
                if (remainingDistance > 0) {
                    remainingDistance = (int) ((double) remainingDistance - distance + 0.5);
                    SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                    editor.putInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, remainingDistance);
                    editor.commit();
                    msgDistance = "\nRemaining distance is " + remainingDistance + " miles";
                } else {
                    Intent dismissIntent = new Intent();
                    dismissIntent.setAction(ACTION_DISMISS);
                    sendBroadcast(dismissIntent);
                }
            }

            if (speed >= ProtexedApp.getInstance().getSharedPreferences().getInt(AppConfig.Prefs.AUTOSTART_SPEED_IN_MPH, 0)) {
                if (!receiversAreWorking) {
                    receiversAreWorking = true;

                    SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                    editor.putBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, true);
                    editor.commit();

                    if (AppConfig.DEBUG)
                        Log.d(TAG, "RECEIVERS ARE WORKING! Excess of the minimum speed");
                }
                updateNotification("Service is running.\nCurrent speed is " + ((int) speed) + " m/ph" + msgDistance);

                if (!isSmsSent && ProtexedApp.getInstance().getSharedPreferences()
                        .getBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true)) {
                    String parentPhone = UserInfo.getParentNumber();
                    if(parentPhone != null && !parentPhone.equals("")) {
                        SettingsFragment.sendSMS(parentPhone, getString(R.string.sms_for_parent));
                        isSmsSent = true;
                    }
                }
            } else {
                if (receiversAreWorking) {
                    receiversAreWorking = false;

                    SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                    editor.putBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false);
                    editor.commit();

                    if (AppConfig.DEBUG)
                        Log.d(TAG, "RECEIVERS ARE STOPPED! Reduction of the minimum speed");
                }
                updateNotification("Service is stopped.\nCurrent speed is " + ((int) speed) + " m/ph" + msgDistance);
            }
        }

        @Override
        public void onError(Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
            Toast.makeText(ProtexedRunningService.this, getResources().getString(R.string.app_name) + " " + getResources().getString(R.string.cant_get_location), Toast.LENGTH_SHORT).show();
        }
    };

    private void updateNotification(String contentText) {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notifyID = R.string.app_name;
        nm.notify(notifyID, initNotification(this, contentText));
    }

    private Notification initNotification(Context context, String contentText) {
        NotificationCompat.Builder ncomp = new NotificationCompat.Builder(context);
        ncomp.setContentTitle(context.getResources().getString(R.string.app_name));
        ncomp.setContentText(contentText);
        ncomp.setTicker(contentText);
        ncomp.setSmallIcon(R.drawable.ic_stat_notify);
        ncomp.setAutoCancel(true);
        ncomp.setStyle(new NotificationCompat.BigTextStyle().bigText(contentText));

        Intent stop = new Intent(this, MainActivity.class);
        stop.putExtra(MainActivity.EXTRA_NOTIFICATION, true);
        stop.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pStop = PendingIntent.getActivity(this, 0, stop, 0);
        ncomp.setContentIntent(pStop);

        Intent dismissIntent = new Intent();
        dismissIntent.setAction(ACTION_DISMISS);
        PendingIntent pDismiss = PendingIntent.getBroadcast(this, 0, dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        ncomp.addAction (R.drawable.ic_location_off_white_24dp, getString(R.string.turn_off), pDismiss);

        Notification notification;
        notification = ncomp.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.priority = Notification.PRIORITY_MAX;
        return notification;
    }

    class DismissReceiver extends BroadcastReceiver {

        @SuppressWarnings("NewApi")
        @Override
        public void onReceive(Context context, Intent intent) {
            if(ACTION_DISMISS.equals(intent.getAction())) {
                Intent stop = new Intent(getApplicationContext(), ProtexedRunningService.class);
                stop.addCategory(ProtexedRunningService.TAG);
                stopService(stop);
            }
        }
    }
}
