package com.clearsofware.protexed.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;

/**
 * Created by Pavel on 4/27/2015.
 */
public class BootReceiver extends BroadcastReceiver {

    public static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (AppConfig.DEBUG)
            Log.d(TAG, "is running");

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) && ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false)) {
            Intent startServiceIntent = new Intent(context, ProtexedRunningService.class);
            startServiceIntent.addCategory(ProtexedRunningService.TAG);
            context.startService(startServiceIntent);
        }
    }
}