package com.clearsofware.protexed.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.database.DBHelper;
import com.clearsofware.protexed.models.DbNotification;

/**
 * Created by Pavel on 4/24/2015.
 */
public class NLService extends NotificationListenerService {

    public static final String NOTIFICATION_LISTENER_SERVICE = "com.clearsofware.protexed.NOTIFICATION_LISTENER_SERVICE";
//    public static final String NOTIFICATION_LISTENER_INTENT = "com.clearsofware.protexed.NOTIFICATION_LISTENER_INTENT";

    private String TAG = "NLService";
//    private NLServiceReceiver mNlservicereciver;

    @Override
    public void onCreate() {
        super.onCreate();
//        mNlservicereciver = new NLServiceReceiver();
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(NOTIFICATION_LISTENER_SERVICE);
//        registerReceiver(mNlservicereciver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(mNlservicereciver);
    }

    @SuppressWarnings("NewApi")
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false)) {

            // SMS
            if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false)
                    && sbn.getPackageName().equals(ProtexedApp.getInstance().getSharedPreferences().getString(AppConfig.Prefs.SMS_DEFAULT_APP_PACKAGE, ""))) {
                processPostedNotif(sbn);
            }
            // EMAIL NOTIFICATION
            if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING, false)
                    && sbn.getPackageName().equals(ProtexedApp.getInstance().getSharedPreferences().getString(AppConfig.Prefs.EMAIL_DEFAULT_APP_PACKAGE, ""))) {
                processPostedNotif(sbn);
            }
            // ANOTHER NOTIFICATION
            if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_NOTIFICATIONS_RECEIVER_RUNNING, false)
                    && !sbn.getPackageName().equals(ProtexedApp.getInstance().getSharedPreferences().getString(AppConfig.Prefs.SMS_DEFAULT_APP_PACKAGE, ""))
                    && !sbn.getPackageName().equals(ProtexedApp.getInstance().getSharedPreferences().getString(AppConfig.Prefs.EMAIL_DEFAULT_APP_PACKAGE, ""))) {
                processPostedNotif(sbn);
            }
        }
    }

    @SuppressWarnings("NewApi")
    private void processPostedNotif(StatusBarNotification sbn) {
        if (AppConfig.DEBUG) {
            Log.d(TAG, "Notification posted. Text: " + sbn.getNotification().tickerText + " Package: " + sbn.getPackageName() + " Is Clearable: " + sbn.isClearable());
        }
//        Intent i = new Intent(NOTIFICATION_LISTENER_INTENT);
//        i.putExtra("notification_event", "onNotificationPosted :" + sbn.getPackageName() + "\n");
//        sendBroadcast(i);

        NLService.this.cancelAllNotifications();

        if (sbn.isClearable())
            DBHelper.getInstance(this).addNotification(new DbNotification(sbn.getPackageName(), (sbn.getNotification()!= null && sbn.getNotification().tickerText!=null ? sbn.getNotification().tickerText.toString() : ""), sbn.getPostTime()));
    }

    @SuppressWarnings("NewApi")
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_PROTEXED_RUNNING, false)) {
            if (AppConfig.DEBUG) {
                Log.d(TAG, "Notification removed. Text: " + sbn.getNotification().tickerText + " Package: " + sbn.getPackageName() + " Is Clearable: " + sbn.isClearable());
            }
//            Intent i = new Intent(NOTIFICATION_LISTENER_INTENT);
//            i.putExtra("notification_event", "onNotificationRemoved :" + sbn.getPackageName() + "\n");
//            sendBroadcast(i);
        }
    }

//    class NLServiceReceiver extends BroadcastReceiver {
//
//        @SuppressWarnings("NewApi")
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getStringExtra("command").equals("clearall")) {
//                NLService.this.cancelAllNotifications();
//            } else if (intent.getStringExtra("command").equals("list")) {
//                Intent i1 = new Intent(NOTIFICATION_LISTENER_INTENT);
//                i1.putExtra("notification_event", "=====================");
//                sendBroadcast(i1);
//                int i = 1;
//                for (StatusBarNotification sbn : NLService.this.getActiveNotifications()) {
//                    Intent i2 = new Intent(NOTIFICATION_LISTENER_INTENT);
//                    i2.putExtra("notification_event", i + " " + sbn.getPackageName() + "\n");
//                    sendBroadcast(i2);
//                    i++;
//                }
//                Intent i3 = new Intent(NOTIFICATION_LISTENER_INTENT);
//                i3.putExtra("notification_event", "===== Notification List ====");
//                sendBroadcast(i3);
//            }
//
//        }
//    }

}