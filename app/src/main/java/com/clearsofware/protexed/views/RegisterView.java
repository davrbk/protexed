package com.clearsofware.protexed.views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.clearsofware.protexed.R;
import com.clearsofware.protexed.activities.RegisterActivity;

/**
 * Created by david on 5/5/15.
 */
public class RegisterView extends RelativeLayout {

    private EditText mLogin, mPas, mAge, mCity, mState, mParentPhone, mWhoTell;

    private Spinner mSex;

    private RegisterViewListener mListener;

    private Context mContext;

    public interface RegisterViewListener {

        void onRegisterButtonClicked(String login, String pas, String age, String sex,
                                     String city,  String state, String parent, String who);

    }

    public RegisterView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
    }

    public void setRegisterViewListener(RegisterViewListener listener) {
        mListener = listener;
    }

    public void removeRegisterViewListener(RegisterViewListener listener) {
        if(listener != null) listener = null;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mLogin       = (EditText) findViewById(R.id.regLoginEdit);

        String twitterName = ((Activity)getContext()).getIntent().getStringExtra("twitter_name");

        if(twitterName == null) {
            Log.d(RegisterActivity.TAG, "Twitter name extra is null");
        } else if(twitterName != null || !twitterName.equals("")) {
            mLogin.setText(twitterName);
        }

        mPas         = (EditText) findViewById(R.id.regPasEdit);

        mAge         = (EditText) findViewById(R.id.regAgeEdit);

        mCity        = (EditText) findViewById(R.id.regCity);

        mState       = (EditText) findViewById(R.id.regState);

        mParentPhone = (EditText) findViewById(R.id.regParentPhoneEdit);

        mWhoTell     = (EditText) findViewById(R.id.regHearEdit);

        mSex         = (Spinner) findViewById(R.id.regSexSpinner);

        mSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.reg_spinner_items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSex.setAdapter(adapter);

        //Setting up register button click listener
        findViewById(R.id.registerButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                synchronized (this) {
                    if(mListener != null) {

                        if(mLogin.getText() == null || mLogin.getText().toString().equals("")
                                || mLogin.getText().toString().length() == 0) {
                            mLogin.requestFocus();
                            Toast.makeText(mContext, "Login must be filled", Toast.LENGTH_SHORT).show();

                        } else if(mPas.getText() == null || mPas.getText().toString().equals("")
                                || mPas.getText().toString().length() == 0) {
                            mPas.requestFocus();
                            Toast.makeText(mContext, "Password must be filled", Toast.LENGTH_SHORT).show();
                        } else if(mParentPhone.getText() == null || mParentPhone.getText().toString().equals("")
                                || mParentPhone.getText().toString().length() == 0) {
                            mParentPhone.requestFocus();
                            Toast.makeText(mContext, "Parent/Guardian phone number must be filled", Toast.LENGTH_SHORT).show();
                        } else {

                            mListener.onRegisterButtonClicked(
                                    mLogin      .getText().toString(),
                                    mPas        .getText().toString(),
                                    mAge        .getText().toString(),
                                    String.valueOf(mSex.getSelectedItemId()),
                                    mCity       .getText().toString(),
                                    mState      .getText().toString(),
                                    mParentPhone.getText().toString(),
                                    mWhoTell    .getText().toString());
                        }
                    }
                }
            }
        });
    }
}
