package com.clearsofware.protexed.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.services.ProtexedRunningService;

/**
 * Created by david on 4/22/15.
 */
public class SettingsView extends RelativeLayout {

    private CheckBox mEmailCheckB, mCallsCheckB, mPopupsCheckB,
                     mSmsCheckB,   mPushCheckB,  mAutoresponseCheckB;

    private EditText mProtexedDrivenMilesEt;

    private SeekBar mSpeedAmountSB;

    private TextView mMilesStepChangingText, mSmsText;

    private Button mInputResponseButton;

    private ToggleButton mEnableDisableButton;

    private SettingsViewListener mSettingsViewListener;

    private String enabledNotificationListeners;

    private SharedPreferences prefs;

    public EditText getProtexedDrivenMilesEt() {
        return mProtexedDrivenMilesEt;
    }

    public interface SettingsViewListener {

        //CheckBoxes callbacks
        void onEmailChecked(CompoundButton buttonView, boolean isChecked);

        void onCallsChecked(CompoundButton buttonView, boolean isChecked);

        void onPopupsChecked(CompoundButton buttonView, boolean isChecked);

        void onSmsChecked(CompoundButton buttonView, boolean isChecked);

        void onPushChecked(CompoundButton buttonView, boolean isChecked);

        void onAutoResponseChecked(CompoundButton buttonView, boolean isChecked);

        //Button callback
        void onButtonClicked(ToggleButton button);

        void onInputResponseButtonClicked();

    }

    public void setSettingsViewListener(SettingsViewListener listener) {
        this.mSettingsViewListener = listener;
    }

    public void removeSettingsViewListener(SettingsViewListener listener) {
        listener = null;
        System.gc();
    }


    public SettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);



        enabledNotificationListeners = Settings.Secure.getString(context.getContentResolver(), "enabled_notification_listeners");
        prefs = ProtexedApp.getInstance().getSharedPreferences();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initViews();

        setupListeners();

        mSpeedAmountSB.incrementProgressBy(5);
        mSpeedAmountSB.setMax(15);
        mSpeedAmountSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress / 5;
                progress = progress * 5;
                mMilesStepChangingText.setText(String.valueOf(progress) + " mp/h");

                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putInt(AppConfig.Prefs.AUTOSTART_SPEED_IN_MPH, progress);
                editor.commit();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        if (prefs != null) {
            mSpeedAmountSB.setProgress(prefs.getInt(AppConfig.Prefs.AUTOSTART_SPEED_IN_MPH, 5));
        }
        mMilesStepChangingText.setText(mSpeedAmountSB.getProgress() + " mp/h");

    }

    private void initViews() {

        //CheckBoxes

        mEmailCheckB = (CheckBox) findViewById(R.id.emailCheckBox);
        if (Build.VERSION.SDK_INT < 18) {
            mEmailCheckB.setVisibility(GONE);
        } else {
            if (prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING, false) &&
                       enabledNotificationListeners != null
                    && enabledNotificationListeners.contains(getContext().getApplicationContext().getPackageName())) {
                mEmailCheckB.setChecked(true);
            }
        }

        mCallsCheckB = (CheckBox) findViewById(R.id.callsCheckBox);
        if (prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_CALLS_RECEIVER_RUNNING, false)) {
            mCallsCheckB.setChecked(true);
        }

        mPopupsCheckB = (CheckBox) findViewById(R.id.popupsCheckBox);
        mPopupsCheckB.setVisibility(GONE);

        mSmsCheckB = (CheckBox) findViewById(R.id.smsCheckBox);
        if (Build.VERSION.SDK_INT >= 18) {
            if (prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false) &&
                    enabledNotificationListeners != null &&
                    enabledNotificationListeners
                            .contains(getContext().getApplicationContext().getPackageName())) {
                mSmsCheckB.setChecked(true);
            }
        } else if (prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false)) {
            mSmsCheckB.setChecked(true);
        }

        mPushCheckB = (CheckBox) findViewById(R.id.pushCheckBox);
        if (Build.VERSION.SDK_INT < 18) {
            mPushCheckB.setVisibility(GONE);
        } else {
            if (prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_NOTIFICATIONS_RECEIVER_RUNNING, false) &&
                    enabledNotificationListeners != null &&
                    enabledNotificationListeners.contains(getContext().getApplicationContext().getPackageName())) {
                mPushCheckB.setChecked(true);
            }
        }

        mAutoresponseCheckB = (CheckBox) findViewById(R.id.autoResposeCheckBox);
        if(prefs != null && prefs.getBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true)) {
            mAutoresponseCheckB.setChecked(true);
        }

        //SeekBar

        mSpeedAmountSB = (SeekBar) findViewById(R.id.speedAmountBar);
        mMilesStepChangingText = (TextView) findViewById(R.id.dynamicMilesText);

        //EditText

        mProtexedDrivenMilesEt = (EditText) findViewById(R.id.drivenMilesEdit);
        mProtexedDrivenMilesEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                if (s.toString().length() > 0) {
                    int distance = Integer.parseInt(s.toString());
                    if (distance == 0) {
                        editor.putInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, -100);
                    } else {
                        editor.putInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, distance);
                    }
                } else {
                    editor.putInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, -100);
                }
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Button

//        mInputResponseButton = (Button) findViewById(R.id.inputResponseButton);

        mEnableDisableButton = (ToggleButton) findViewById(R.id.enableDisableButton);
        if (ProtexedRunningService.isServiceWorking())
            mEnableDisableButton.setChecked(true);

        mSmsText = (TextView) findViewById(R.id.textSms);

    }

    private void setupListeners() {

        mEmailCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onEmailChecked(buttonView, isChecked);
                    }
                }
            }
        });

        mCallsCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onCallsChecked(buttonView, isChecked);
                    }
                }
            }
        });

        mPopupsCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onPopupsChecked(buttonView, isChecked);
                    }
                }
            }
        });

        mSmsCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onSmsChecked(buttonView, isChecked);
                    }
                }
            }
        });

        mSmsText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onInputResponseButtonClicked();
                    }
                }
            }
        });

        mPushCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onPushChecked(buttonView, isChecked);
                    }
                }
            }
        });

        mAutoresponseCheckB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onAutoResponseChecked(buttonView, isChecked);
                    }
                }
            }
        });

//        mInputResponseButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                synchronized (this) {
//                    if(mSettingsViewListener != null) {
//                        mSettingsViewListener.onInputResponseButtonClicked();
//                    }
//                }
//            }
//        });

        mEnableDisableButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this) {
                    if (mSettingsViewListener != null) {
                        mSettingsViewListener.onButtonClicked((ToggleButton) v);
                    }
                }
            }
        });

    }
}
