package com.clearsofware.protexed.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.clearsofware.protexed.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiException;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.internal.TwitterApiConstants;

import java.util.ArrayList;

/**
 * Created by david on 4/27/15.
 */
public class LoginView extends RelativeLayout {

    private EditText mLogin, mPas;

    private LoginViewListener mListener;

    private Context mContext;

    public interface LoginViewListener {

        void onSignInClicked(String login, String pas);

        void onSignUpClicked();
    }

    public void setLoginViewListener(LoginViewListener listener) {
        mListener = listener;
    }

    public void removeLoginViewListener() {

        if(mListener != null) {
            mListener = null;
        }
    }


    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mLogin = (EditText) findViewById(R.id.profileLoginEdit);

        mPas   = (EditText) findViewById(R.id.profilePasswordEdit);

        findViewById(R.id.profileSignButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                synchronized (this) {
                    if (mListener != null) {

                        if (mLogin.getText() == null || mLogin.getText().toString().equals("")
                                || mLogin.getText().toString().length() == 0) {
                            mLogin.requestFocus();
                            Toast.makeText(mContext, "Login must be filled", Toast.LENGTH_SHORT).show();

                        } else if (mPas.getText() == null || mPas.getText().toString().equals("")
                                || mPas.getText().toString().length() == 0) {
                            mPas.requestFocus();
                            Toast.makeText(mContext, "Password must be filled", Toast.LENGTH_SHORT).show();
                        } else {
                            mListener.onSignInClicked(mLogin.getText().toString(), mPas.getText().toString());
                        }
                    }
                }
            }
        });

        findViewById(R.id.profileSignUpButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this) {
                    if (mListener != null) {
                        mListener.onSignUpClicked();
                    }
                }
            }
        });
    }
}
