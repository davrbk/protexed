package com.clearsofware.protexed.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.clearsofware.protexed.R;
import com.clearsofware.protexed.objects.UserInfo;

/**
 * Created by david on 5/11/15.
 */
public class ProfileView extends RelativeLayout {

    private EditText mLoginEdit, mAgeEdit, mParentEdit, mAboutEdit, mCityEdit, mStateEdit;

    private Spinner mSex;

    private Context mContext;

    private ProfileViewListener mListener;

    public interface ProfileViewListener {

        void onUpdateButtonClicked(String login, String age, String sex, String parent,
                                   String city,  String state, String about);
    }

    public void setProfileViewListener(ProfileViewListener listener) {
        mListener = listener;
    }

    public ProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mLoginEdit = (EditText) findViewById(R.id.profileLoginEdit);
        mLoginEdit.setText(UserInfo.getLogin());

        mAgeEdit = (EditText) findViewById(R.id.profileAgeEdit);
        mAgeEdit.setText(UserInfo.getAge());

        mSex = (Spinner) findViewById(R.id.profileSexSpinner);

        mParentEdit = (EditText) findViewById(R.id.profileParentEdit);
        mParentEdit.setText(UserInfo.getParentNumber());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.reg_spinner_items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSex.setAdapter(adapter);

//        mParentEdit = (EditText) findViewById(R.id.profileParentEdit);
//        mParentEdit.setText(UserInfo.getParentNumber());

        mAboutEdit= (EditText) findViewById(R.id.profileAboutEdit);
        mAboutEdit.setText(UserInfo.getAbout());

        mCityEdit = (EditText) findViewById(R.id.profileCityEdit);
        mCityEdit.setText(UserInfo.getCity());

        mStateEdit = (EditText) findViewById(R.id.profileStateEdit);
        mStateEdit.setText(UserInfo.getState());

        findViewById(R.id.updateButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this) {
                    if(mListener != null) {
                        if(mLoginEdit.getText() == null || mLoginEdit.getText().toString().equals("")
                                || mLoginEdit.getText().toString().length() == 0) {
                            mLoginEdit.requestFocus();
                            Toast.makeText(mContext, "Login must be filled", Toast.LENGTH_SHORT).show();

                        } else if (mParentEdit.getText() == null || mParentEdit.getText().toString().equals("")
                                || mParentEdit.getText().toString().length() == 0) {
                            mParentEdit.requestFocus();
                            Toast.makeText(mContext, "Parent number must be filled", Toast.LENGTH_SHORT).show();

                        } else {

                            mListener.onUpdateButtonClicked(
                                    mLoginEdit      .getText().toString(),
                                    mAgeEdit        .getText().toString(),
                                    String.valueOf(mSex.getSelectedItemId()),
                                    mParentEdit     .getText().toString(),
                                    mCityEdit       .getText().toString(),
                                    mStateEdit      .getText().toString(),
                                    mAboutEdit      .getText().toString());
                        }
                    }
                }

            }
        });

    }
}
