package com.clearsofware.protexed.activities;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.json.JsonApi;
import com.clearsofware.protexed.objects.UserInfo;
import com.clearsofware.protexed.utils.NetworkUtils;
import com.clearsofware.protexed.views.RegisterView;

/**
 * Created by david on 5/5/15.
 */
public class RegisterActivity extends ActionBarActivity implements RegisterView.RegisterViewListener {

    private RegisterView mView;

    public static String TAG = "RegisterActivity";

    @Override
    protected void onStop() {
        super.onStop();

        if(mView != null) mView.removeRegisterViewListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mView = (RegisterView) getLayoutInflater().inflate(R.layout.activity_register, null, false);

        if(mView != null) {
            mView.setRegisterViewListener(this);
        }

        setContentView(mView);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRegisterButtonClicked(final String login,  final String pas,
                                        final String age,    final String sex,
                                        final String city,   final String state,
                                        final String parent, final String who) {
        if(NetworkUtils.isOnline(this, true)) {

            new AsyncTask<String, Void, Void>() {

                @Override
                protected Void doInBackground(String... urls) {

                    JsonApi.registerUser(login, pas, age, sex, city, state, parent, who, urls[0]);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    UserInfo.setLogin(       login);
                    UserInfo.setPas(         pas);
                    UserInfo.setAge(         age);
                    UserInfo.setSex(         sex);
                    UserInfo.setParentNumber(parent);
                    UserInfo.setAbout(       who);
                    UserInfo.setCity(        city);
                    UserInfo.setState(       state);

                    NavUtils.navigateUpFromSameTask(RegisterActivity.this);
                }

            }.execute(JsonApi.getSignupUrl());

        } else {
            Toast.makeText(this, "Connection lost!", Toast.LENGTH_SHORT).show();
        }
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, parent);
        editor.commit();
    }
}
