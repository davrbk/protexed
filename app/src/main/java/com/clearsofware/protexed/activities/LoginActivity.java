package com.clearsofware.protexed.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.fragments.LoginFragment;

public class LoginActivity extends ActionBarActivity {

    public static String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (ProtexedApp.getInstance().getSharedPreferences().getString(AppConfig.Prefs.LOCAL_USER, "").length() > 0) {
            startActivity(new Intent(this, MainActivity.class));
            super.onCreate(savedInstanceState);
            finish();
        } else {
            setTheme(R.style.MainActivityTheme);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);

        if(f != null) {
            f.onActivityResult(requestCode, resultCode, data);
        }
    }
}
