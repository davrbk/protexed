package com.clearsofware.protexed.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.FrameLayout;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.fragments.HistoryFragment;
import com.clearsofware.protexed.fragments.LocationTestFragment;
import com.clearsofware.protexed.fragments.LoginFragment;
import com.clearsofware.protexed.fragments.NavigationDrawerFragment;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.fragments.ProfileFragment;
import com.clearsofware.protexed.fragments.SettingsFragment;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static final String EXTRA_NOTIFICATION = "com.clearsofware.protexed.activities.MainActivity.EXTRA_NOTIFICATION";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    DrawerLayout drawer;

    /**
     * Used to store the last screen title. For use in {link restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent != null && intent.getBooleanExtra(EXTRA_NOTIFICATION, false)) {
            NavigationDrawerFragment.setCurrentSelectedPosition(1); // Settings fragment
        }

//        stopService(new Intent(NLService.NOTIFICATION_LISTENER_SERVICE));

        // Inflate the "decor.xml"
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        drawer = (DrawerLayout) inflater.inflate(R.layout.fragment_navigation_drawer_hack, null); // "null" is important.

        // HACK: "steal" the first child of decor view
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        View child = decor.getChildAt(0);
        decor.removeView(child);
        FrameLayout container = (FrameLayout) drawer.findViewById(R.id.container); // This is the container we defined just now.
        container.addView(child);

        // Make the drawer replace the first child
        decor.addView(drawer);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();
        setTitle(mTitle.toString().toUpperCase());

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = null;
        String tag = null;

        if (position == 0) {

            boolean isLogged = ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.LOGGED, false);

            if(isLogged && !(getCurrentFragment(R.id.container) instanceof ProfileFragment)) {

                fragment = new ProfileFragment();
                tag = ProfileFragment.TAG;
                setTitle(getResources().getStringArray(R.array.drawer_items)[position].toUpperCase());

            } else if (!isLogged && !(getCurrentFragment(R.id.container) instanceof LoginFragment)) {

                fragment = new LoginFragment();
                tag = LoginFragment.TAG;
                setTitle(getResources().getStringArray(R.array.drawer_items)[position].toUpperCase());
            }

        } else if (position == 1 && !(getCurrentFragment(R.id.container) instanceof SettingsFragment)) {

            fragment = new SettingsFragment();
            tag = SettingsFragment.TAG;
            setTitle(getResources().getStringArray(R.array.drawer_items)[position].toUpperCase());

        } else if (position == 2 && !(getCurrentFragment(R.id.container) instanceof LocationTestFragment)) {

//            fragment = new LocationTestFragment();
            fragment = new HistoryFragment();
            tag = HistoryFragment.TAG;
            setTitle(getResources().getStringArray(R.array.drawer_items)[position].toUpperCase());
        }

        if (fragment != null) {
//            fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            fragmentManager.beginTransaction().replace(R.id.container, fragment).addToBackStack(tag).commitAllowingStateLoss();
            fragmentManager.beginTransaction().replace(R.id.container, fragment, tag).commitAllowingStateLoss();
        }

    }

    private Fragment getCurrentFragment( int id) {
        return getSupportFragmentManager().findFragmentById(id);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            drawer.closeDrawers();
//        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
//            this.finish();
        } else {
            super.onBackPressed();
        }
    }
}
