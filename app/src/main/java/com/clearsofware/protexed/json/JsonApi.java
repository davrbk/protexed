package com.clearsofware.protexed.json;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 5/7/15.
 */
public class JsonApi {

    private static String SIGNUP_URL = "http://digi-app.com/json.php";
    private static String SIGNIN_URL = "http://digi-app.com/json.php";

    public static String getSignupUrl() {
        return SIGNUP_URL;
    }

    public static String getSigninUrl() {
        return SIGNIN_URL;
    }

    public static void registerUser(String login,  String pas,   String age,
                                    String sex,    String city,  String state,
                                    String parent, String who,   String url) {

        JSONParser jParser = new JSONParser();

        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("protexed_signup", "1"));
        params.add(new BasicNameValuePair("login",        login));
        params.add(new BasicNameValuePair("password",     pas));
        params.add(new BasicNameValuePair("age",          age));
        params.add(new BasicNameValuePair("sex",          sex));
        params.add(new BasicNameValuePair("City",         city));
        params.add(new BasicNameValuePair("State",        state));
        params.add(new BasicNameValuePair("parentNumber", parent));
        params.add(new BasicNameValuePair("about",        who));

        jParser.makeHttpRequest(url, "GET", params);

    }

    public static void updateUser(String login,  String pas,      String age,
                                  String sex,    String parent,   String city,
                                  String state,  String who,      String refCode,
                                  String url) {

        JSONParser jParser = new JSONParser();

        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("protexed_userupdate", "1"));
        params.add(new BasicNameValuePair("login",        login));
        params.add(new BasicNameValuePair("password",     pas));
        params.add(new BasicNameValuePair("age",          age));
        params.add(new BasicNameValuePair("sex",          sex));
        params.add(new BasicNameValuePair("parentNumber", parent));
        params.add(new BasicNameValuePair("about",        who));
        params.add(new BasicNameValuePair("City",         city));
        params.add(new BasicNameValuePair("State",        state));
        params.add(new BasicNameValuePair("RefCode",      refCode));

        jParser.makeHttpRequest(url, "GET", params);

    }

    public static JSONArray loginUser(String login, String pas, String url) {

        JSONParser jParser = new JSONParser();

        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("protexed_signin", "1"));
        params.add(new BasicNameValuePair("login",        login));
        params.add(new BasicNameValuePair("password",     pas));

        return jParser.makeHttpRequest(url, "GET", params);
    }
}
