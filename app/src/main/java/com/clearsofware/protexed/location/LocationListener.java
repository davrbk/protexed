package com.clearsofware.protexed.location;

/**
 * Created by Pavel on 4/22/2015.
 */
public interface LocationListener {

    public void onLocationReceived(double speed, double distance,double lat, double lng);
    public void onError(Exception e);
}
