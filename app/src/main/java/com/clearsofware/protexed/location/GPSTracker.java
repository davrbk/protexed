package com.clearsofware.protexed.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;

public class GPSTracker extends Service {

    public static final String TAG = "GPSTracker";

    private final Context mContext;
    private final OnTrackerChangedListener onTrackerChangedListener;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context, OnTrackerChangedListener onTrackerChangedListener) {
        this.mContext = context;
        this.onTrackerChangedListener = onTrackerChangedListener;
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // getting network status
//            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled) {
                // no GPS provider is enabled
                // showSettingsAlert();
            } else {
                this.canGetLocation = true;

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, AppConfig.Location.MIN_TIME_BW_UPDATES, AppConfig.Location.MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);
                    if (AppConfig.DEBUG)
                        Log.d(TAG, "GPS Enabled, getting location");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // get Location from Network Provider
//                if (isNetworkEnabled) {
//                    if (location == null) {
//                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, AppConfig.Location.MIN_TIME_BW_UPDATES, AppConfig.Location.MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);
//                        if (AppConfig.DEBUG)
//                            Log.d(TAG, "Getting location via network");
//                        if (locationManager != null) {
//                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                            if (location != null) {
//                                latitude = location.getLatitude();
//                                longitude = location.getLongitude();
//                            }
//                        }
//                    }
//                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (AppConfig.DEBUG) {
                Log.d(TAG, "Location Changed " + location.getLatitude() + " " + location.getLongitude());
            }
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            onTrackerChangedListener.onLocationChanged(location.getLatitude(), location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (AppConfig.DEBUG)
                Log.d(TAG, "Status Changed");
            onTrackerChangedListener.onStatusChanged(provider, status, extras);
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (AppConfig.DEBUG)
                Log.d(TAG, "Provider Enabled");
            onTrackerChangedListener.onProviderEnabled(provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (AppConfig.DEBUG)
                Log.d(TAG, "Provider Disabled");
            onTrackerChangedListener.onProviderDisabled(provider);
        }
    };

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(listener);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
//        if (location != null) {
//            latitude = location.getLatitude();
//        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
//        if (location != null) {
//            longitude = location.getLongitude();
//        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public interface OnTrackerChangedListener {
        public void onProviderDisabled(String provider);
        public void onLocationChanged(double lat, double lng);
        public void onProviderEnabled(String provider);
        public void onStatusChanged(String provider, int status, Bundle extras);
    }
}
