package com.clearsofware.protexed.location;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;

/**
 * Created by Pavel on 4/22/2015.
 */
public class GetLocation extends AsyncTask<Void, Void, GetLocation.LatLng> {

    public static final String TAG = "GetLocation";
    private static final float METERS_PER_SECOND_TO_MILES_PER_HOUR = (float) 2.23694;
    private static final double METERS_TO_MILES = 0.000621371192;

    private Context mContext;
    private GPSTracker mGpsTracker;
    private LocationListener mLocationListener;
    private boolean withPopup;

    private static Double mPrevLat;
    private static Double mPrevLng;

    public GetLocation(Context context, GPSTracker gpsTracker, LocationListener locationListener, boolean withPopup) {
        mContext = context;
        mGpsTracker = gpsTracker;
        mLocationListener = locationListener;
        this.withPopup = withPopup;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (withPopup) {
            // dosmth
        }
    }

    @Override
    protected LatLng doInBackground(Void... params) {
        if (mGpsTracker.canGetLocation()) {
            double latitude = mGpsTracker.getLatitude();
            double longitude = mGpsTracker.getLongitude();
            if (AppConfig.DEBUG)
                Log.d(TAG, "Current location " + latitude + " " + longitude);
            return new LatLng(latitude, longitude);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(LatLng latLng) {
        super.onPostExecute(latLng);
        try {
            if (withPopup) {
                // dosmth
            }

            if (latLng == null)
                mLocationListener.onError(null);
            else {
                double speed = 0.0;
                double distanceInMiles = 0.0;
                if (mPrevLat != null && mPrevLng != null) {
                    double distanceInMeters = distance(new LatLng(mPrevLat, mPrevLng), latLng);
                    distanceInMiles = distanceInMeters * METERS_TO_MILES;
                    double timeInSeconds = AppConfig.Location.MIN_TIME_BW_UPDATES / 1000;
                    speed = distanceInMeters / timeInSeconds;
                }
//                mPrevLat = latLng.getLat() + Math.random();
//                mPrevLng = latLng.getLng() + Math.random();
                mPrevLat = latLng.getLat();
                mPrevLng = latLng.getLng();

                double speedInMph = speed * METERS_PER_SECOND_TO_MILES_PER_HOUR;

                mLocationListener.onLocationReceived(speedInMph, distanceInMiles, latLng.getLat(), latLng.getLng());
            }
        } catch (Exception e) {
            e.printStackTrace();
            mLocationListener.onError(e);
        }
    }

    private double distance(LatLng prev, LatLng curr) {
        int R = 6371000;
        double dLat = toRad(curr.getLat() - prev.getLat());
        double dLon = toRad(curr.getLng() - prev.getLng());
        double lat1 = toRad(prev.getLat());
        double lat2 = toRad(curr.getLat());
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        return d;
    }

    private double toRad(Double d) {
        return d * Math.PI / 180;
    }

    public static class LatLng {
        double lat;
        double lng;

        LatLng(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }
    }
}