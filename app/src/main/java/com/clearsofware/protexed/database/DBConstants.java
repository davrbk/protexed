package com.clearsofware.protexed.database;

public class DBConstants {

    public static DBHelper dbHelper					    		        		= null;
    public static final String	DATABASE_NAME									= "protexed.sqlite";
    public static final int	DATABASE_VERSION									= 1;

    public static final String	INTEGER											= " INTEGER ";
    public static final String	TEXT											= " TEXT ";
    public static final String	DATE											= " DATETIME ";
    public static final String	REAL                                            = " REAL ";
    public static final String  INTEGER_KEY                                     = " INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String  INTEGER_NOT_NULL                                = " INTEGER NOT NULL";
    public static final String  TEXT_NOT_NULL                                   = " TEXT NOT NULL";

    public static final String	COUNT_ROWS                                      = "COUNT_ROWS";
    public static final String	SELECT_COUNT_FROM                               = "SELECT COUNT(*) AS " + COUNT_ROWS + " FROM ";
    public static final String	SELECT_ALL_FROM                                 = "SELECT * FROM ";
    public static final String	WHERE                                           = " WHERE ";
    public static final String	AND                                             = " AND ";
    public static final String	OR                                              = " OR ";
    public static final String	UPDATE                                          = "UPDATE ";
    public static final String	SELECT                                          = "SELECT ";
    public static final String	SET                                             = " SET ";
    public static final String	LIKE                                            = " LIKE ";
    public static final String	FROM                                            = " FROM ";

    // ********************** notification table *************************

    public static final String TABLE_NAME_NOTIFICATIONS = "protexed_notifications";

    public final static String NOTIFICATIONS_COLUMN_ID = "_id";
    public final static String NOTIFICATIONS_COLUMN_APP_PACKAGE = "package";
    public final static String NOTIFICATIONS_COLUMN_TICKER = "ticker";
    public final static String NOTIFICATIONS_COLUMN_TIME = "time";
    public final static String NOTIFICATIONS_COLUMN_IS_READ = "is_read";

    public static final String CREATE_TABLE_NOTIFICATIONS = "create table IF NOT EXISTS " + TABLE_NAME_NOTIFICATIONS + " ( " +
            NOTIFICATIONS_COLUMN_ID +                   INTEGER_KEY           + ", " +
            NOTIFICATIONS_COLUMN_APP_PACKAGE +          TEXT_NOT_NULL         + ", " +
            NOTIFICATIONS_COLUMN_TICKER +               TEXT                  + ", " +
            NOTIFICATIONS_COLUMN_TIME +                 INTEGER_NOT_NULL      + ", " +
            NOTIFICATIONS_COLUMN_IS_READ +              INTEGER_NOT_NULL      + " ) ";

    // ********************** notification table *************************
}
