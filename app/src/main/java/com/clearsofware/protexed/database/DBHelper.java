package com.clearsofware.protexed.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.models.DbNotification;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper implements DBHandler {

    public static final String TAG = "DATABASE";

    public DBHelper(Context context) {
        super(context, DBConstants.DATABASE_NAME, null, DBConstants.DATABASE_VERSION);
    }

    static DBHelper singleton;

    public synchronized static DBHelper getInstance(Context context) {
        if (null == singleton) {
            singleton = new DBHelper(context);
        }
        return singleton;
    }

    // ********************************DATABASE**********************************************************************************************************************************************************************************************************************
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DBConstants.CREATE_TABLE_NOTIFICATIONS);
            if (AppConfig.DEBUG) {
                String textSucc = "Table [" + DBConstants.TABLE_NAME_NOTIFICATIONS + "] was created successfully";
                Log.d(TAG, textSucc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy(SQLiteDatabase db) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + DBConstants.CREATE_TABLE_NOTIFICATIONS);
            if (AppConfig.DEBUG) {
                String textSucc = "Table [" + DBConstants.TABLE_NAME_NOTIFICATIONS + "] was destroyed successfully";
                Log.d(TAG, textSucc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            onDestroy(db);
            onCreate(db);
        }
    }
    // ********************************NOTIFICATIONS*****************************************************************************************************************************************************************************************************************


    @Override
    public void addNotification(DbNotification notification) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBConstants.NOTIFICATIONS_COLUMN_APP_PACKAGE, notification.getAppPackage());
        if (notification.getTicker() != null)
            cv.put(DBConstants.NOTIFICATIONS_COLUMN_TICKER, notification.getTicker());
        cv.put(DBConstants.NOTIFICATIONS_COLUMN_TIME, notification.getTime() != null ? notification.getTime() : System.currentTimeMillis());
        cv.put(DBConstants.NOTIFICATIONS_COLUMN_IS_READ, 0);
        db.insert(DBConstants.TABLE_NAME_NOTIFICATIONS, null, cv);
        db.close();
    }

    @Override
    public ArrayList<DbNotification> getAllNotifications(Boolean isRead) {
        ArrayList<DbNotification> list = new ArrayList<>();
        String selectQuery = DBConstants.SELECT_ALL_FROM + DBConstants.TABLE_NAME_NOTIFICATIONS + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                list.add(new DbNotification(cursor.getString(1), cursor.getString(2), cursor.getLong(3), cursor.getInt(4)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return list;
    }

    @Override
    public void updateIsRead(long time, boolean isRead) {
        int i = (isRead ? 1 : 0);
        SQLiteDatabase db = this.getWritableDatabase();
        String selectClientQuery = "update " + DBConstants.TABLE_NAME_NOTIFICATIONS + " set " + DBConstants.NOTIFICATIONS_COLUMN_IS_READ + " = " + i + " where " + DBConstants.NOTIFICATIONS_COLUMN_TIME + " = " + time + ";";
        Cursor cursor = db.rawQuery(selectClientQuery, null);
        cursor.moveToFirst();
        cursor.close();
        db.close();
    }

    @Override
    public void removeNotification(long time) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DBConstants.TABLE_NAME_NOTIFICATIONS, DBConstants.NOTIFICATIONS_COLUMN_TIME + " = ?", new String[]{String.valueOf(time)});
        db.close();
    }

    // ********************************CALENDAR*************************************************************************************************************************************************************************************************************************
}
