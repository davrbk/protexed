package com.clearsofware.protexed.database;

import android.database.sqlite.SQLiteDatabase;

import com.clearsofware.protexed.models.DbNotification;

import java.util.ArrayList;

public interface DBHandler {

    public void onDestroy(SQLiteDatabase db);

    public void addNotification(DbNotification notification);

    public ArrayList<DbNotification> getAllNotifications(Boolean isRead);

    public void updateIsRead(long time, boolean isRead);

    public void removeNotification(long time);
}
