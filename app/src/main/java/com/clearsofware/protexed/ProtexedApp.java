package com.clearsofware.protexed;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

public class ProtexedApp extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "urIHsMLIgGUamDL9r1jU1NidB";
//    private static final String TWITTER_SECRET = "I5rMXkSHkpoVSNIB7CFG0jpIsZoRaqD5pmGpJwLxiMuCoNL9Nl";

    private static final String TWITTER_KEY = "HwcDUjEhdph6fcr2JouluqzlV";
    private static final String TWITTER_SECRET = "iVckblZLmeCvIM5gLPM5a8MGaw1h2cMh5dDqvblyw0fkJPfNkJ";


    private static ProtexedApp instance;
    private static SharedPreferences sharedPreferences;
    public static Context context;

    public static ProtexedApp getInstance() {
        if (instance == null) {
            instance = new ProtexedApp();
        }
        return instance;
    }

    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        context = getApplicationContext();

//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = getSharedPreferences(AppConfig.Prefs.PREFS_NAME, Context.MODE_PRIVATE);
    }
}
