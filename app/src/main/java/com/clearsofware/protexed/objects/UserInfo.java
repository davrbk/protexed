package com.clearsofware.protexed.objects;

import android.content.SharedPreferences;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;

/**
 * Created by david on 6/5/15.
 */
public class UserInfo {

    private static String ID;

    private static String LOGIN;

    private static String PAS;

    private static String AGE;

    private static String SEX;

    private static String PARENT_NUMBER;

    private static String ABOUT;

    private static String CITY;

    private static String STATE;

    private static String REF_CODE;

    public static String getId() {

        return ProtexedApp.getInstance().getSharedPreferences()
                                .getString(AppConfig.Prefs.USER_ID, ID);
    }

    public static void setId(String id) {
        UserInfo.ID = id;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_ID, id);
        editor.commit();
    }

    public static String getLogin() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_LOGIN, LOGIN);
    }

    public static void setLogin(String login) {
        UserInfo.LOGIN = login;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_LOGIN, login);
        editor.commit();
    }

    public static String getPas() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_PAS, PAS);
    }

    public static void setPas(String pas) {
        UserInfo.PAS = pas;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_PAS, pas);
        editor.commit();
    }

    public static String getAge() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_AGE, AGE);
    }

    public static void setAge(String age) {
        UserInfo.AGE = age;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_AGE, age);
        editor.commit();
    }

    public static String getSex() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_SEX, SEX);
    }

    public static void setSex(String sex) {
        UserInfo.SEX = sex;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_SEX, sex);
        editor.commit();
    }

    public static String getParentNumber() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, PARENT_NUMBER);
    }

    public static void setParentNumber(String parentNumber) {
        PARENT_NUMBER = parentNumber;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, parentNumber);
        editor.commit();
    }

    public static String getAbout() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_ABOUT, ABOUT);
    }

    public static void setAbout(String about) {
        UserInfo.ABOUT = about;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_ABOUT, about);
        editor.commit();
    }

    public static String getCity() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_CITY, CITY);
    }

    public static void setCity(String city) {
        UserInfo.CITY = city;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_CITY, city);
        editor.commit();
    }

    public static String getState() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_STATE, STATE);
    }

    public static void setState(String state) {
        UserInfo.STATE = state;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_STATE, state);
        editor.commit();
    }

    public static String getRefCode() {
        return ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.USER_REF_CODE, REF_CODE);
    }

    public static void setRefCode(String refCode) {
        REF_CODE = refCode;
        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
        editor.putString(AppConfig.Prefs.USER_REF_CODE, refCode);
        editor.commit();
    }
}
