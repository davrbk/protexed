package com.clearsofware.protexed.fragments;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.objects.UserInfo;
import com.clearsofware.protexed.receivers.PopupsBroadcastReceiver;
import com.clearsofware.protexed.services.ProtexedRunningService;
import com.clearsofware.protexed.receivers.CallsBroadcastReceiver;
import com.clearsofware.protexed.utils.DisplayUtils;
import com.clearsofware.protexed.receivers.SmsBroadcastReceiver;
import com.clearsofware.protexed.views.SettingsView;

/**
 * Created by david on 4/24/15.
 */
public class SettingsFragment extends Fragment implements SettingsView.SettingsViewListener {

    public static final String TAG = "SettingsFragment";

    private SettingsView mSettingsView;

    private CallsBroadcastReceiver mCallsBroadcastReceiver;

    private SmsBroadcastReceiver mSmsReceiver;

    private PopupsBroadcastReceiver mPopupReceiver;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mSettingsView = (SettingsView) inflater.inflate(R.layout.fragment_settings, null, false);
        mSettingsView.setPadding(0, DisplayUtils.getStatusBarHeight(getActivity()) + DisplayUtils.getActionBarHeight(getActivity()), 0, 0);

        if (mSettingsView != null) {
            mSettingsView.setSettingsViewListener(this);
        }

        return mSettingsView;
    }

    @Override
    public void onEmailChecked(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {

            String enabledNotificationListeners = Settings.Secure.getString(getActivity().getContentResolver(), "enabled_notification_listeners");
            String defaultApplication           = "com.google.android.gm";

            if (enabledNotificationListeners == null
                    || !enabledNotificationListeners.contains(getActivity().getApplicationContext().getPackageName())) {

                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                buttonView.setChecked(false);

//                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
//                editor.putBoolean(AppConfig.Prefs.IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING, true);
//                editor.putString(AppConfig.Prefs.EMAIL_DEFAULT_APP_PACKAGE, defaultApplication);
//                editor.commit();

            } else {
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putBoolean(AppConfig.Prefs.IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING, true);
                editor.putString(AppConfig.Prefs.EMAIL_DEFAULT_APP_PACKAGE, defaultApplication);
                editor.commit();
            }
        } else {
            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
            editor.putBoolean(AppConfig.Prefs.IS_EMAIL_NOTIFICATIONS_RECEIVER_RUNNING, false);
            editor.commit();
        }

    }

    @Override
    public void onCallsChecked(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            mCallsBroadcastReceiver = new CallsBroadcastReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.intent.action.PHONE_STATE");
            filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
            filter.setPriority(100000);
            getActivity().registerReceiver(mCallsBroadcastReceiver, filter);
            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
            editor.putBoolean(AppConfig.Prefs.IS_CALLS_RECEIVER_RUNNING, true).commit();
            if (AppConfig.DEBUG)
                Log.d("Call receiver", "registered");
        } else {
            if (mCallsBroadcastReceiver != null) {
                try {
                    getActivity().unregisterReceiver(mCallsBroadcastReceiver);
                    mCallsBroadcastReceiver = null;
                    if (AppConfig.DEBUG)
                        Log.d("Call receiver", "unregistered");
                } catch (IllegalArgumentException e) {
                    if (AppConfig.DEBUG)
                        Log.d("Call receiver", "was not registered");
                }
            }
            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
            editor.putBoolean(AppConfig.Prefs.IS_CALLS_RECEIVER_RUNNING, false).commit();
        }
    }

    @Override
    public void onPopupsChecked(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            mPopupReceiver = new PopupsBroadcastReceiver();
            IntentFilter intentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            intentFilter.setPriority(2147483647);
            getActivity().registerReceiver(mPopupReceiver, intentFilter);
            if (AppConfig.DEBUG)
                Log.d("Popups receiver", "registered");

//            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
//            getActivity().sendBroadcast(closeDialog);
        } else {
            if (mPopupReceiver != null) {
                try {
                    getActivity().unregisterReceiver(mPopupReceiver);
                    mPopupReceiver = null;
                    if (AppConfig.DEBUG)
                        Log.d("Popups receiver", "unregistered");

                } catch (IllegalArgumentException e) {

                    if (AppConfig.DEBUG)
                        Log.d("Popups receiver", "was not registered");
                }
            }
        }
    }

    @Override
    public void onSmsChecked(CompoundButton buttonView, boolean isChecked) {

        if (Build.VERSION.SDK_INT < 18) {

            if (isChecked) {

                registerSmsReceiver();
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, true).commit();
                if (AppConfig.DEBUG)
                    Log.d("SMS receiver", "registered");
            } else {
                unregisterSmsReceiver();
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false).commit();
            }

        } else {

            if (isChecked) {

                String enabledNotificationListeners = Settings.Secure.getString(getActivity().getContentResolver(), "enabled_notification_listeners");
                String defaultApplication           = Settings.Secure.getString(getActivity().getContentResolver(), "sms_default_application");

                if (enabledNotificationListeners == null
                        || !enabledNotificationListeners.contains(getActivity().getApplicationContext().getPackageName())) {

                    startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                    buttonView.setChecked(false);
                } else {
                    if (AppConfig.DEBUG)
                        Log.d(TAG, "Default SMS app\'s package is " + (defaultApplication != null ? defaultApplication : "null"));

                    registerSmsReceiver();
                    SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                    editor.putBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, true).commit();
                    if (defaultApplication != null)
                        editor.putString(AppConfig.Prefs.SMS_DEFAULT_APP_PACKAGE, defaultApplication).commit();
                }
            } else {
                unregisterSmsReceiver();
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putBoolean(AppConfig.Prefs.IS_SMS_RECEIVER_RUNNING, false).commit();
            }
        }
    }

    @Override
    public void onPushChecked(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            String enabledNotificationListeners = Settings.Secure.getString(getActivity().getContentResolver(), "enabled_notification_listeners");
            if (enabledNotificationListeners == null
                    || !enabledNotificationListeners.contains(getActivity().getApplicationContext().getPackageName())) {

                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                buttonView.setChecked(false);

            } else {
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putBoolean(AppConfig.Prefs.IS_NOTIFICATIONS_RECEIVER_RUNNING, true);
                editor.commit();
            }
        } else {
            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
            editor.putBoolean(AppConfig.Prefs.IS_NOTIFICATIONS_RECEIVER_RUNNING, false);
            editor.commit();
        }
    }

    @Override
    public void onAutoResponseChecked(CompoundButton buttonView, boolean isChecked) {

        SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();

        if(isChecked) {
            editor.putBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true);
            editor.commit();
        } else {
            editor.putBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, false);
            editor.commit();
        }
    }

    @Override
    public void onButtonClicked(ToggleButton button) {

        if (button.isChecked()) {
            String miles = mSettingsView.getProtexedDrivenMilesEt().getText().toString();
            if (miles.length() > 0 && Integer.parseInt(miles) > 0) {
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putInt(AppConfig.Prefs.PROTEXED_DRIVEN_MILES, Integer.parseInt(miles));
                editor.commit();
            }

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Intent startServiceIntent = new Intent(getActivity().getApplicationContext(), ProtexedRunningService.class);
                startServiceIntent.addCategory(ProtexedRunningService.TAG);
                getActivity().startService(startServiceIntent);
            } else {
                button.setChecked(false);
                showSettingsAlert();
            }
        } else {
            Intent intent = new Intent(getActivity().getApplicationContext(), ProtexedRunningService.class);
            intent.addCategory(ProtexedRunningService.TAG);
            getActivity().stopService(intent);

            //Sending message to parent / guardian after stopping service
//            String parentPhone = ProtexedApp.getInstance().getSharedPreferences()
//                    .getString(AppConfig.Prefs.USER_GUARDIAN_PARENT_PHONE, "");
            if (ProtexedApp.getInstance().getSharedPreferences().getBoolean(AppConfig.Prefs.IS_AUTORESPONSE_RUNNING, true)) {
                String parentPhone = UserInfo.getParentNumber();
                if(parentPhone != null && !parentPhone.equals("")) {
                    SettingsFragment.sendSMS(parentPhone, getString(R.string.sms_for_parent));
                }
            }
        }
    }

    @Override
    public void onInputResponseButtonClicked() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Input message");

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);

        final EditText input = new EditText(getActivity());
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        input.setLayoutParams(lp);
        layout.setPadding(10, 0, 10, 0);
        input.setHint("Hint");
        layout.addView(input);

        String message = ProtexedApp.getInstance().getSharedPreferences()
                .getString(AppConfig.Prefs.AUTORESPONSE_MESSAGE, getString(R.string.sms_protexed));
        input.setText(message);
        alertDialog.setView(layout);

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                editor.putString(AppConfig.Prefs.AUTORESPONSE_MESSAGE, input.getText().toString());
                editor.commit();
            }
        });
        alertDialog.show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("We would like to use your location");
        alertDialog.setNegativeButton("Don't allow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (!provider.contains("gps")) { // if gps is disabled
                    final Intent poke = new Intent();
                    poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                    poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                    poke.setData(Uri.parse("3"));
                    getActivity().sendBroadcast(poke);
                }
            }
        });
        alertDialog.show();
    }

    public static void sendSMS(String phoneNumber, String message) {

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }

    private void registerSmsReceiver() {

        mSmsReceiver = new SmsBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter.setPriority(2147483647);
        getActivity().registerReceiver(mSmsReceiver, filter);
    }

    private void unregisterSmsReceiver() {
        if (mSmsReceiver != null) {
            try {
                getActivity().unregisterReceiver(mSmsReceiver);
                mSmsReceiver = null;
                if (AppConfig.DEBUG)
                    Log.d("SMS receiver", "unregistered");
            } catch (IllegalArgumentException e) {
                if (AppConfig.DEBUG)
                    Log.d("SMS receiver", "was not registered");
            }
        }
    }

    private boolean checkNotificationSetting() {

        ContentResolver contentResolver = getActivity().getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = getActivity().getPackageName();

        return !(enabledNotificationListeners == null || !enabledNotificationListeners.contains(packageName));
    }
}

