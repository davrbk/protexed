package com.clearsofware.protexed.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.activities.LoginActivity;
import com.clearsofware.protexed.activities.MainActivity;
import com.clearsofware.protexed.activities.RegisterActivity;
import com.clearsofware.protexed.json.JsonApi;
import com.clearsofware.protexed.objects.UserInfo;
import com.clearsofware.protexed.utils.DisplayUtils;
import com.clearsofware.protexed.utils.NetworkUtils;
import com.clearsofware.protexed.views.LoginView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiException;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.internal.TwitterApiConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pavel on 4/24/2015.
 */
public class LoginFragment extends Fragment implements LoginView.LoginViewListener {

    public static final String TAG = "LoginFragment";

    private CallbackManager callbackManager;

    private LoginButton mFacebookLogin;

    public TwitterLoginButton mTwitterLogin;

    private LoginView mView;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Init Facebook SDK
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        mView = (LoginView) inflater.inflate(R.layout.fragment_login, null);

        if(getActivity() instanceof MainActivity) {
            mView.setPadding(0, DisplayUtils.getStatusBarHeight(getActivity()) + DisplayUtils.getActionBarHeight(getActivity()), 0, 0);
        }

        if(mView != null) {
            mView.setLoginViewListener(this);
        }

        callbackManager = CallbackManager.Factory.create();

        //Init Twitter login button
        initTwitterLoginButton();

        //Init FB login button
        initFbLoginButton();

        return mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mTwitterLogin.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSignInClicked(String login, String pas) {

            loginUserAsync(login, pas);
    }

    @Override
    public void onSignUpClicked() {

        Intent i = new Intent(getActivity(), RegisterActivity.class);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();

//        if(mView != null) {
//            mView.removeLoginViewListener();
//        }
    }

    private void loginUserAsync(final String login, final String pas) {

        if(NetworkUtils.isOnline(getActivity(), true)) {

            final ProgressDialog progress = new ProgressDialog(getActivity());
            progress.setTitle("Logging in...");

            new AsyncTask<String, Void, JSONArray>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
//                    progress.show();
                }

                @Override
                protected JSONArray doInBackground(String... urls) {
                    return JsonApi.loginUser(login, pas, urls[0]);
                }

                @Override
                protected void onPostExecute(JSONArray jsonArray) {
                    super.onPostExecute(jsonArray);
//                    progress.dismiss();

                    JSONObject jObj = null;

                    if (jsonArray != null) {

                        try {
                            jObj = jsonArray.getJSONObject(0);

                            UserInfo.setId(          jObj.getString("id"));
                            UserInfo.setLogin(       jObj.getString("login"));
                            UserInfo.setPas(         jObj.getString("password"));
                            UserInfo.setAge(         jObj.getString("age"));
                            UserInfo.setSex(         jObj.getString("sex"));
                            UserInfo.setParentNumber(jObj.getString("parentNumber"));
                            UserInfo.setAbout(       jObj.getString("about"));
                            UserInfo.setCity(        jObj.getString("City"));
                            UserInfo.setState(       jObj.getString("State"));
                            UserInfo.setRefCode(     jObj.getString("RefCode"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(getActivity() instanceof LoginActivity) {

                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.container, new ProfileFragment()).commit();

                            getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();

                            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                            editor.putBoolean(AppConfig.Prefs.LOGGED, true);
                            editor.putString(AppConfig.Prefs.LOCAL_USER, "Test");
                            editor.commit();

                        } else {

                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.container, new ProfileFragment()).commit();

                            SharedPreferences.Editor editor = ProtexedApp.getInstance().getSharedPreferences().edit();
                            editor.putBoolean(AppConfig.Prefs.LOGGED, true);
                            editor.commit();
                        }

                    } else {
                        Toast.makeText(getActivity(), "This login or password not exists!", Toast.LENGTH_SHORT).show();
                    }
                }

            }.execute(JsonApi.getSigninUrl());

        } else {
            Toast.makeText(getActivity(), "Connection lost!", Toast.LENGTH_SHORT).show();
        }
    }

    private void initFbLoginButton() {

        mFacebookLogin = (LoginButton) mView.findViewById(R.id.loginFacebook);
        mFacebookLogin.setReadPermissions("user_friends");
        mFacebookLogin.setFragment(this);
        // Callback registration
        mFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getActivity(), "Connection lost", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initTwitterLoginButton() {

        mTwitterLogin = (TwitterLoginButton)
                mView.findViewById(R.id.loginTwitter);

        mTwitterLogin.setCallback(new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a
                // TwitterSession for making API calls

                TwitterSession data = result.data;
                Intent i = new Intent(getActivity(), RegisterActivity.class);
                i.putExtra("twitter_name", data.getUserName());
                startActivity(i);

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure

                final TwitterApiException apiException = (TwitterApiException) exception;
                final int errorCode = apiException.getErrorCode();
                if (errorCode == TwitterApiConstants.Errors.APP_AUTH_ERROR_CODE
                        || errorCode == TwitterApiConstants.Errors.GUEST_AUTH_ERROR_CODE) {
                    // get new guestAppSession
                    // optionally retry
                }
            }
        });
    }
}
