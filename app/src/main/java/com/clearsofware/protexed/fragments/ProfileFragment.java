package com.clearsofware.protexed.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.clearsofware.protexed.AppConfig;
import com.clearsofware.protexed.ProtexedApp;
import com.clearsofware.protexed.R;
import com.clearsofware.protexed.json.JsonApi;
import com.clearsofware.protexed.objects.UserInfo;
import com.clearsofware.protexed.utils.DisplayUtils;
import com.clearsofware.protexed.utils.NetworkUtils;
import com.clearsofware.protexed.views.ProfileView;

/**
 * Created by david on 5/11/15.
 */
public class ProfileFragment extends Fragment implements ProfileView.ProfileViewListener{

    public static String TAG = "ProfileFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ProfileView v = (ProfileView) inflater.inflate(R.layout.fragment_profile, null);
        v.setPadding(0, DisplayUtils.getStatusBarHeight(getActivity()) + DisplayUtils.getActionBarHeight(getActivity()), 0, 0);

        if(v != null) {
            v.setProfileViewListener(this);
        }
        return v;
    }

    @Override
    public void onUpdateButtonClicked(final String login,  final String age, final String sex,
                                      final String parent, final String city,
                                      final String state,  final String about) {

        if(NetworkUtils.isOnline(getActivity(), true)) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("Enter password");

            LinearLayout layout = new LinearLayout(getActivity());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER_HORIZONTAL);

            final EditText input = new EditText(getActivity());
            layout.setPadding(10, 0, 10, 0);
            input.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            input.setHint("Password");
            layout.addView(input);
            alertDialog.setView(layout);

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(input.getText().toString().equals(ProtexedApp.getSharedPreferences()
                            .getString(AppConfig.Prefs.USER_PAS, ""))) {
                        updateUserInfoAsync(age, sex, parent, city, state, about);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Password is not correct!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            alertDialog.show();

        } else {
            Toast.makeText(getActivity(), "Connection lost!", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateUserInfoAsync(final String age,    final String sex,
                                     final String parent, final String city,
                                     final String state,  final String about) {

        new AsyncTask<String, Void, Void>() {

            @Override
            protected Void doInBackground(String... urls) {

                JsonApi.updateUser(UserInfo.getLogin(), UserInfo.getPas(), age, sex, parent, city, state, about, UserInfo.getRefCode(), urls[0]);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

//                    UserInfo.setLogin(       login);
                UserInfo.setAge(         age);
                UserInfo.setSex(         sex);
                UserInfo.setAbout(       about);
                UserInfo.setCity(        city);
                UserInfo.setState(       state);
//                    NavUtils.navigateUpFromSameTask(RegisterActivity.this);
                Toast.makeText(getActivity(), "User info is successfully updated!", Toast.LENGTH_SHORT).show();
            }

        }.execute(JsonApi.getSignupUrl());

    }
}
