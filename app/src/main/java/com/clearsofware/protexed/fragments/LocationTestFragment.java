package com.clearsofware.protexed.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clearsofware.protexed.R;
import com.clearsofware.protexed.location.GPSTracker;
import com.clearsofware.protexed.location.LocationListener;
import com.clearsofware.protexed.location.GetLocation;
import com.clearsofware.protexed.utils.DisplayUtils;

/**
 * Created by pavel on 26.12.2014.
 */
public class LocationTestFragment extends Fragment {

    public static final String TAG = "LocationTestFragment";
    private GPSTracker mGpsTracker;

    private LinearLayout mLayout;
    private TextView mSpeedTv;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_location, null);
        view.setPadding(0, DisplayUtils.getStatusBarHeight(getActivity()) + DisplayUtils.getActionBarHeight(getActivity()), 0, 0);

        mLayout = (LinearLayout) view.findViewById(R.id.layout);

        mSpeedTv = (TextView) view.findViewById(R.id.speed);

        mGpsTracker = new GPSTracker(getActivity(), new GPSTracker.OnTrackerChangedListener() {
            @Override
            public void onProviderDisabled(String provider) {
                showSettingsAlert();
            }

            @Override
            public void onLocationChanged(double lat, double lng) {
                new GetLocation(getActivity(), mGpsTracker, mLocationListener, false).execute();
            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        });

        // call it when you need
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new GetLocation(getActivity(), mGpsTracker, mLocationListener, false).execute();
        } else showSettingsAlert();

        return view;
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationReceived(double speed, double distance, double lat, double lng) {
            if (LocationTestFragment.this.isAdded()) {
                mSpeedTv.setText(String.valueOf(speed).substring(0, 3) + " m/s");
                Toast.makeText(getActivity(), "New location received", Toast.LENGTH_SHORT).show();

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                TextView tv = new TextView(getActivity());
                tv.setLayoutParams(p);
                tv.setText(lat + " " + lng);
                mLayout.addView(tv);
            }
        }

        @Override
        public void onError(Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
            if (LocationTestFragment.this.isAdded())
                Toast.makeText(getActivity(), getResources().getString(R.string.cant_get_location), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mGpsTracker != null) {
            mGpsTracker.stopUsingGPS();
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("We would like to use your location");
        alertDialog.setNegativeButton("Don't allow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (!provider.contains("gps")) { // if gps is disabled
                    final Intent poke = new Intent();
                    poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                    poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
                    poke.setData(Uri.parse("3"));
                    getActivity().sendBroadcast(poke);
                }
            }
        });
        alertDialog.show();
    }
}