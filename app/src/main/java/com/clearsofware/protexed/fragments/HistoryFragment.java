package com.clearsofware.protexed.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.clearsofware.protexed.R;
import com.clearsofware.protexed.database.DBHelper;
import com.clearsofware.protexed.models.DbNotification;
import com.clearsofware.protexed.utils.DisplayUtils;
import com.clearsofware.protexed.utils.IOUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by pavel on 26.12.2014.
 */
public class HistoryFragment extends Fragment {

    public static final String TAG = "HistoryFragment";

    private ListView mListView;
    private TextView mEmptyList;

    private ArrayAdapter<DbNotification> mAdapter;
    private ArrayList<DbNotification> mArrayList;
    private Vibrator mVibrator;
    private PackageManager mPackageManager;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, null);
        view.setPadding(0, DisplayUtils.getStatusBarHeight(getActivity()) + DisplayUtils.getActionBarHeight(getActivity()), 0, 0);

        mPackageManager = getActivity().getApplicationContext().getPackageManager();

        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        mEmptyList = (TextView) view.findViewById(R.id.empty_list);

        mListView = (ListView) view.findViewById(R.id.items);
        mListView.addFooterView(new View(getActivity()));
        mListView.addHeaderView(initHeaderView(), null, false);

        mArrayList = DBHelper.getInstance(getActivity()).getAllNotifications(null);
        Collections.sort(mArrayList, new NotificationComparator());

        if (mArrayList.size() == 0) {
            mEmptyList.setVisibility(View.VISIBLE);
        } else {
            mAdapter = new ArrayAdapter<DbNotification>(getActivity(), R.layout.item_history, mArrayList) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    DbNotification data = getItem(position);

                    ViewHolder holder;
                    View itemView = convertView;

                    if (itemView == null) {
                        itemView = inflater.inflate(R.layout.item_history, null);
                        holder = new ViewHolder();
                        holder.main = (TextView) itemView.findViewById(R.id.main);
                        holder.additional = (TextView) itemView.findViewById(R.id.additional);
                        holder.time = (TextView) itemView.findViewById(R.id.time);
                        holder.indicator = itemView.findViewById(R.id.indicator);

                        itemView.setTag(holder);
                    } else {
                        holder = (ViewHolder) itemView.getTag();
                    }

                    if (data.getAppPackage().equals(DbNotification.APP_PHONE)) {
                        holder.main.setText(getResources().getString(R.string.missed_call));
                    } else if (data.getAppPackage() != null) {

                        if(Build.VERSION.SDK_INT < 18 && data.getAppPackage().equals(DbNotification.APP_SMS)) {
                            holder.main.setText(getResources().getString(R.string.income_sms));
                        } else {
                            holder.main.setText(getAppName(data.getAppPackage()));
                        }

                    } else {
                        holder.main.setText(data.getAppPackage());
                    }

                    holder.time.setText(IOUtils.getDate(data.getTime(), IOUtils.DATE_FORMAT_HISTORY));

                    if (data.getTicker() != null) {

                        if(Build.VERSION.SDK_INT < 18 && data.getAppPackage().equals(DbNotification.APP_SMS)) {

                            String[] s = data.getTicker().split(",");
                            holder.additional.setText(s[1]);

                        } else {
                            holder.additional.setText(data.getTicker());
                        }

                    } else {
                        holder.additional.setText("");
                    }

                    if (!data.isRead()) {
                        holder.indicator.setBackgroundColor(getResources().getColor(R.color.history_indicator_color));
                    } else {
                        holder.indicator.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    }

                    return itemView;
                }
            };
            mListView.setAdapter(mAdapter);
            registerForContextMenu(mListView);

            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final DbNotification data = (DbNotification) parent.getAdapter().getItem(position);
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle(((TextView) view.findViewById(R.id.main)).getText());
                    if (data.getTicker() != null) {

                        if(Build.VERSION.SDK_INT < 18 && data.getAppPackage().equals(DbNotification.APP_SMS)) {

                            String[] s = data.getTicker().split(",");
                            alertDialog.setMessage(s[1]);

                        } else {
                            alertDialog.setMessage(data.getTicker());
                        }
                    }
                    alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    if (getAppName(data.getAppPackage()) != null || data.getAppPackage().equals(DbNotification.APP_PHONE)) {
                        alertDialog.setPositiveButton(getResources().getString(data.getAppPackage().equals(DbNotification.APP_PHONE) ? R.string.recall : R.string.open_app), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (data.getAppPackage().equals(DbNotification.APP_PHONE)) {
                                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + (data.getTicker() != null ? data.getTicker() : "")));
                                    startActivity(intent);
                                } else {
                                    openApp(getActivity(), data.getAppPackage());
                                }
                                dialog.dismiss();
                            }
                        });

                    } else if (Build.VERSION.SDK_INT < 18 && data.getAppPackage().equals(DbNotification.APP_SMS)) {

                        alertDialog.setPositiveButton(R.string.open_app, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String[] s = data.getTicker().split(",");

                                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                                smsIntent.setType("vnd.android-dir/mms-sms");
                                smsIntent.putExtra("address", s[0]);
//                                smsIntent.putExtra("sms_body",s[1]);
                                startActivity(smsIntent);
                            }
                        });
                    }
                    alertDialog.show();
                    data.setRead(true);
                    DBHelper.getInstance(getActivity()).updateIsRead(data.getTime(), true);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.items) {
            mVibrator.vibrate(50);
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_histiry_fragment, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        DbNotification data = (DbNotification) mListView.getItemAtPosition(info.position);
        switch (item.getItemId()) {
            case R.id.remove:
                for (int i = 0; i < mArrayList.size(); i++) {
                    if (mArrayList.get(i).getTime() == data.getTime()) {
                        mArrayList.remove(mArrayList.get(i));
                        mAdapter.notifyDataSetChanged();
                        if (mArrayList.size() == 0)
                            mEmptyList.setVisibility(View.VISIBLE);
                        DBHelper.getInstance(getActivity()).removeNotification(data.getTime());
                        Toast.makeText(getActivity(), getResources().getString(R.string.item_removed), Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                return true;
            case R.id.read:
                data.setRead(true);
                DBHelper.getInstance(getActivity()).updateIsRead(data.getTime(), true);
                mAdapter.notifyDataSetChanged();
                return true;
            case R.id.unread:
                data.setRead(false);
                DBHelper.getInstance(getActivity()).updateIsRead(data.getTime(), false);
                mAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private View initHeaderView() {
        View v = new View(getActivity());
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtils.dpToPx(getActivity(), 16));
        v.setLayoutParams(params);
        return v;
    }

    static class ViewHolder {
        public TextView main;
        public TextView additional;
        public TextView time;
        public View indicator;
    }

    private String getAppName(String packageNAme) {
        ApplicationInfo ai;
        try {
            ai = mPackageManager.getApplicationInfo(packageNAme, 0);
        } catch (final PackageManager.NameNotFoundException e) {
            ai = null;
        }
        final String applicationName = (ai != null ? mPackageManager.getApplicationLabel(ai).toString() : null);
        return applicationName;
    }

    private void openApp(Context context, String packageName) {
        try {
            Intent i = mPackageManager.getLaunchIntentForPackage(packageName);
            if (i != null) {
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                context.startActivity(i);
            }
        } catch (Exception e) {
            Toast.makeText(context, getResources().getString(R.string.app_not_found), Toast.LENGTH_SHORT).show();
        }
    }

    private class NotificationComparator implements Comparator<DbNotification> {
        @Override
        public int compare(DbNotification o1, DbNotification o2) {
            return (-o1.getTime().compareTo(-o2.getTime()));
        }
    }
}